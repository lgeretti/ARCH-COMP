function Hf=hessianTensor_SUTRACovid_fixed(x,u)



 Hf{1} = sparse(7,7);

Hf{1}(3,1) = -1/4;
Hf{1}(4,1) = -1/4;
Hf{1}(1,3) = -1/4;
Hf{1}(1,4) = -1/4;


 Hf{2} = sparse(7,7);

Hf{2}(3,2) = -1/4;
Hf{2}(4,2) = -1/4;
Hf{2}(2,3) = -1/4;
Hf{2}(2,4) = -1/4;


 Hf{3} = sparse(7,7);

Hf{3}(3,1) = 1/4;
Hf{3}(4,1) = 1/4;
Hf{3}(1,3) = 1/4;
Hf{3}(1,4) = 1/4;


 Hf{4} = sparse(7,7);

Hf{4}(3,2) = 1/4;
Hf{4}(4,2) = 1/4;
Hf{4}(2,3) = 1/4;
Hf{4}(2,4) = 1/4;


 Hf{5} = sparse(7,7);



 Hf{6} = sparse(7,7);

