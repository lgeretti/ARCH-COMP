function msg = getErrorMessage(id)

% input preprocessing
if ~ischar(id)
    error("Error message identifier has to be a char array.");
end

% enable access to codex
global codex;

% check if id in codex
ididx = find(ismember(codex.id,id));

% empty message if id cannot be found
if isempty(ididx)
    msg = '';
else
    msg = codex.text{ididx};
end

end