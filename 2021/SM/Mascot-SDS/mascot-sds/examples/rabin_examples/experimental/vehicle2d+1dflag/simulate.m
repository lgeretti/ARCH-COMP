%
% simulate.m
%
% created on: 09.10.2015
%     author: rungger (vehicle.m)
%     modified by: kaushik
%
%
% you need to run ./vehicle binary first 
%
% so that the file: vehicle_controller_under.bdd is created
%

function simulate
    clear set
    close all
    addpath(genpath('../../../../mfiles'))

    %% simulation
    openfig('vehicle_domain')
    hold on
    office = [0.1,0.9,1.1,1.9];
    % initial state
    x0=[1.28000000000000,1.5,1];
    r0=1; % initial state of the rabin automaton
    eps = 0.00001;
    % number of rabin states
    nr = 5;
    
    
%     % real obstacle rectangle in the format [x y w h]
%     avoid = [0.8, 1.0, 0.4, 0.4];
% 
%     % real target rectangle in the format [x y w h]
%     target = [0.8, 0.2, 0.4, 0.4];
% 
    % state space bounds in the form [xmin xmax ymin ymax]
    xlim = [0 2 0 2];

    % parameters for the simulation
    [~,~,W_ub,~,~,tau] = readParams('input.hh');

    % prepare the figure window
%     figure
    axis(xlim);

    % load the symbolic sets containing the controllers
    C=cell(1,nr);
    for ii=1:nr
        C{1,ii}=SymbolicSet(['controllers/C' num2str(ii) '.bdd']);
    end
    % load the symbolic set containing the rabin transition function
    rabin_tr=SymbolicSet('rabin/transition.bdd');

%     % plot the initial state, target, and obstacles
%     plot(x0(1),x0(2),'k*');
%     rectangle('Position',target,'FaceColor',[51, 204, 51]/255);
%     rectangle('Position',avoid,'FaceColor','k');
%     % label the initial state, target, and obstacles
%     text(0.94,2.44,'$I$','interpreter','latex','FontSize',16);
%     text(0.74,0.94,'$A$','interpreter','latex','FontSize',16);
%     text(0.75,0.12,'$B$','interpreter','latex','FontSize',16);
    
    req = rectangle('Position',[0.5, 1.5, 0.1, 0.1],'FaceColor','none');
    
    % simulate T time-steps
    y=x0;
    v=[];
    % simulate the rabin automaton
    s=[r0;rabin_tr.getInputs([r0, x0])];
    T=1000; 
    for t=1:T
        disp(t)
        u=C{1,s(end)}.getInputs(y(end,:));

        v=[v; u(1,:)];
        x = vehicle(y(end,:),v(end,:)); 
        r = rabin_tr.getInputs([s(end),x])
        
        if (x(3)==1)
            req.FaceColor='r';
        else
            req.FaceColor='none';
        end

        y=[y; x(end,:)];
        s=[s;r];
        plot(y(end-1:end,1),y(end-1:end,2),'r.-');
        h1 = plot(y(end,1),y(end,2),'ko','MarkerSize',10);

        pause(0.1)
        delete(h1);
    end

    savefig('traj_stochastic');
    
    function status = atOffice(x)
        if (x(1)-office(1)>eps...
            && x(2)-office(2)>eps...
            && x(1)-(office(1)+office(3))<eps...
            && x(2)-(office(2)+office(4))<eps)
            status = true;
        else
            status = false;
        end
    end

    function xn = vehicle(x,u)
%         x(3)
        % uniform random noise
        w = 2*W_ub(1:2).*(rand(1,2))' - W_ub(1:2);
        % worst case noise
    %         w = wmax;
        xn = zeros(size(x));
        xn(1) = x(1) + u(1)*tau + w(1);
        xn(2) = x(2) + u(2)*tau + w(2);
        if (x(3)==0)
            toss=rand;
            if (toss>0.5)
                xn(3)=1;
            end
        elseif (atOffice(x))
            xn(3)=0;
        else 
            xn(3)=x(3);
        end
    end

end



