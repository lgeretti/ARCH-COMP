import re
import pandas as pd

# set up regular expressions
# use https://regexper.com to visualise these if required
rx_dict = {
    'time': re.compile(r'real\t(?P<time>.*)\n'),
    'volume': re.compile(r'Final volume : (?P<volume>.*)\n'),
    'cnt': re.compile(r'cnt : (?P<cnt>.*)\n'),
    'diam': re.compile(r'Diam x4 : (?P<diam>.*)\n'),
    'time2': re.compile(r'Time taken by function: (?P<time2>.*) seconds\n'),
    'diam2': re.compile(r'diam (?P<diam2>.*)\n')
}

def _parse_line(line):

    for key, rx in rx_dict.items():
        match = rx.search(line)
        if match:
            return key, match
    # if there are no matches
    return None, None

def parse_file(filepath):

    data = []  # create an empty list to collect the data
    # open the file and read through it line by line
    with open(filepath, 'r') as file_object:
        line = file_object.readline()
        while line:
            # at each line check for a match with a regex
            key, match = _parse_line(line)
	    
            # extract school name
            if key == 'time':
                #print  (match.group('time'))
                data.append(match.group('time'))

            # extract grade
            if key == 'volume':
                #print  (match.group('volume'))
                data.append(match.group('volume'))

            # identify a table header 
            if key == 'cnt':
            	#print  (match.group('cnt'))
            	data.append(match.group('cnt'))
            	
            # identify a table header 
            if key == 'diam':
            	#print  (match.group('diam'))
            	data.append(match.group('diam'))
            	
            if key == 'time2':
               #print  (match.group('time2'))
               data.append(match.group('time2'))
            if key == 'diam2':
               #print  (match.group('diam2'))
               data.append(match.group('diam2'))
               
            line = file_object.readline()

    return data
    
if __name__ == '__main__':

    output_file=open('dynibex.csv', 'w')

    #filepath = 'output_docker/pds_stage1-stats.txt'
    #data = parse_file(filepath)
    #print(data)
    #output_file.write('DynIbex,PRD20,I,1,'+data[1]+','+data[0]+'\n')
    
    #filepath = 'output_docker/pds_stage2-stats.txt'
    #data = parse_file(filepath)
    #print(data)
    #output_file.write('DynIbex,PRD20,P,1,'+data[1]+','+data[0]+'\n')
    #filepath = 'output_docker/pds_stage3-stats.txt'
    #data = parse_file(filepath)
    #print(data)    
    #output_file.write('DynIbex,PRD20,IP,1,'+data[1]+','+data[0]+'\n')
    
    filepath = 'output_docker/rob-stats.txt'
    data = parse_file(filepath)
    print(data)
    output_file.write('DynIbex,ROBE21,I,1,'+data[0]+','+data[3]+'\n')
    output_file.write('DynIbex,ROBE21,I,2,'+data[1]+','+data[4]+'\n')
    output_file.write('DynIbex,ROBE21,I,3,'+data[2]+','+data[5]+'\n')
    
    filepath = 'output_docker/cvdp-mu1-stats.txt'
    data = parse_file(filepath)
    print(data)
    output_file.write('DynIbex,CVDP20,mu1,1,'+data[0]+'\n')
    #ajout ligne vide pour mu2
    output_file.write('DynIbex,CVDP20,mu2,0,-\n')
    
    
    filepath = 'output_docker/llt-stats.txt'
    data = parse_file(filepath)
    print(data)
    output_file.write('DynIbex,LALO20,W001,1,'+data[1]+','+data[0]+'\n')
    filepath = 'output_docker/llm-stats.txt'
    data = parse_file(filepath)
    output_file.write('DynIbex,LALO20,W005,1,'+data[1]+','+data[0]+'\n')
    print(data)
    filepath = 'output_docker/lll-stats.txt'
    data = parse_file(filepath)
    output_file.write('DynIbex,LALO20,W01,1,'+data[1]+','+data[0]+'\n')
    print(data)
    
    filepath = 'output_docker/q1-stats.txt'
    data = parse_file(filepath)
    output_file.write('DynIbex,QUAD20,delta01,1,'+data[1]+','+data[0]+'\n')
    print(data)
    filepath = 'output_docker/q4-stats.txt'
    data = parse_file(filepath)
    output_file.write('DynIbex,QUAD20,delta04,1,'+data[1]+','+data[0]+'\n')
    print(data)
    filepath = 'output_docker/q8-stats.txt'
    data = parse_file(filepath)
    output_file.write('DynIbex,QUAD20,delta08,1,'+data[1]+','+data[0]+'\n')
    print(data)
    
    filepath = 'output_docker/hlv-stats.txt'
    data = parse_file(filepath)
    output_file.write('DynIbex,LOVO20,,1,'+data[2]+','+data[1]+','+data[0]+'\n')
    print(data)
    
    filepath = 'output_docker/srdv-stats.txt'
    data = parse_file(filepath)
    output_file.write('DynIbex,SPRE20,,1,'+data[1]+'\n')
    print(data)
    

