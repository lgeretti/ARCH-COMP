function add2params(name,status,checkfuncs,varargin)
% function add2params(name,status,checkfuncs,errmsgs,varargin)
%ADD2PARAMS Summary of this function goes here
%   Detailed explanation goes here

global fullParamsList;

if isempty(varargin)
    fullParamsList = add2list(fullParamsList,name,status,checkfuncs);
%     fullParamsList = add2list(fullParamsList,name,status,checkfuncs,errmsgs);
else
    fullParamsList = add2list(fullParamsList,name,status,checkfuncs,varargin{1});
%     fullParamsList = add2list(fullParamsList,name,status,checkfuncs,errmsgs,varargin{1});
end

end

