--------------------------------------------------------
ARCH'23 AINNCS Category
Tool: CORA - A Tool for Continuous Reachability Analysis
Date: 30-Mar-2023 15:02:37
--------------------------------------------------------
 
Running 20 Benchmarks.. 
 
--------------------------------------------------------
BENCHMARK: Sherlock-Benchmark 10 (Unicycle Car Model)
Time to compute random simulations: 1.1466
Time to check violation in simulations: 1.7187
...compute symbolic Jacobian
...compute symbolic Jacobian (output)
...compute symbolic Hessians
dynamics dim 1
dynamics dim 2
dynamics dim 3
dynamics dim 4
dynamics dim 5
dynamics dim 6
Time to compute reachable set: 3.2971
Time to check verification: 0.04194
Total Time: 6.2044
Result: VERIFIED
Plotting..
 
'01_Unicycle' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Adaptive Cruise Controller (ACC)
Time to compute random simulations: 0.96427
Time to check violation in simulations: 0.01122
...compute symbolic Jacobian
...compute symbolic Jacobian (output)
...compute symbolic Hessians
dynamics dim 1
dynamics dim 2
dynamics dim 3
dynamics dim 4
dynamics dim 5
dynamics dim 6
dynamics dim 7
Time to compute reachable set: 2.3899
Time to check verification: 0.049235
Total Time: 3.4146
Result: VERIFIED
Plotting..
 
'02_ACC' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Sherlock-Benchmark-9 (TORA)
Time to compute random simulations: 0.44677
Time to check violation in simulations: 0.7977
...compute symbolic Jacobian
...compute symbolic Jacobian (output)
...compute symbolic Hessians
dynamics dim 1
dynamics dim 2
dynamics dim 3
dynamics dim 4
dynamics dim 5
...compute symbolic third-order derivatives
dynamic index 1,1
dynamic index 1,2
dynamic index 1,3
dynamic index 1,4
dynamic index 1,5
dynamic index 1,6
dynamic index 2,1
dynamic index 2,2
dynamic index 2,3
dynamic index 2,4
dynamic index 2,5
dynamic index 2,6
dynamic index 3,1
dynamic index 3,2
dynamic index 3,3
dynamic index 3,4
dynamic index 3,5
dynamic index 3,6
dynamic index 4,1
dynamic index 4,2
dynamic index 4,3
dynamic index 4,4
dynamic index 4,5
dynamic index 4,6
dynamic index 5,1
dynamic index 5,2
dynamic index 5,3
dynamic index 5,4
dynamic index 5,5
dynamic index 5,6
dynamic index 1,1
dynamic index 1,2
dynamic index 1,3
dynamic index 1,4
dynamic index 1,5
dynamic index 1,6
dynamic index 2,1
dynamic index 2,2
dynamic index 2,3
dynamic index 2,4
dynamic index 2,5
dynamic index 2,6
dynamic index 3,1
dynamic index 3,2
dynamic index 3,3
dynamic index 3,4
dynamic index 3,5
dynamic index 3,6
dynamic index 4,1
dynamic index 4,2
dynamic index 4,3
dynamic index 4,4
dynamic index 4,5
dynamic index 4,6
dynamic index 5,1
dynamic index 5,2
dynamic index 5,3
dynamic index 5,4
dynamic index 5,5
dynamic index 5,6
Time to compute reachable set: 12.1176
Time to check verification: 0.71955
Total Time: 14.0816
Result: VERIFIED
Plotting..
 
'03_TORA_default' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: TORA Heterogeneous (tanh)
Time to compute random simulations: 0.48837
Time to check violation in simulations: 0.47891
...compute symbolic Jacobian
...compute symbolic Jacobian (output)
...compute symbolic Hessians
dynamics dim 1
dynamics dim 2
dynamics dim 3
dynamics dim 4
dynamics dim 5
...compute symbolic third-order derivatives
dynamic index 1,1
dynamic index 1,2
dynamic index 1,3
dynamic index 1,4
dynamic index 1,5
dynamic index 1,6
dynamic index 2,1
dynamic index 2,2
dynamic index 2,3
dynamic index 2,4
dynamic index 2,5
dynamic index 2,6
dynamic index 3,1
dynamic index 3,2
dynamic index 3,3
dynamic index 3,4
dynamic index 3,5
dynamic index 3,6
dynamic index 4,1
dynamic index 4,2
dynamic index 4,3
dynamic index 4,4
dynamic index 4,5
dynamic index 4,6
dynamic index 5,1
dynamic index 5,2
dynamic index 5,3
dynamic index 5,4
dynamic index 5,5
dynamic index 5,6
dynamic index 1,1
dynamic index 1,2
dynamic index 1,3
dynamic index 1,4
dynamic index 1,5
dynamic index 1,6
dynamic index 2,1
dynamic index 2,2
dynamic index 2,3
dynamic index 2,4
dynamic index 2,5
dynamic index 2,6
dynamic index 3,1
dynamic index 3,2
dynamic index 3,3
dynamic index 3,4
dynamic index 3,5
dynamic index 3,6
dynamic index 4,1
dynamic index 4,2
dynamic index 4,3
dynamic index 4,4
dynamic index 4,5
dynamic index 4,6
dynamic index 5,1
dynamic index 5,2
dynamic index 5,3
dynamic index 5,4
dynamic index 5,5
dynamic index 5,6
Time to compute reachable set: 11.1092
Time to check verification: 0.053674
Total Time: 12.1301
Result: VERIFIED
Plotting..
 
'04_TORA_heterogeneousTanh' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: TORA Heterogeneous (sigmoid)
Time to compute random simulations: 0.3912
Time to check violation in simulations: 0.60895
Time to compute reachable set: 8.244
Time to check verification: 0.027708
Total Time: 9.2718
Result: VERIFIED
Plotting..
 
'05_TORA_heterogeneousSigmoid' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Single Pendulum
Time to compute random simulations: 0.44644
Time to check violation in simulations: 0.054273
...compute symbolic Jacobian
...compute symbolic Jacobian (output)
...compute symbolic Hessians
dynamics dim 1
dynamics dim 2
dynamics dim 3
Time to compute reachable set: 1.8198
Time to check verification: 0.025171
Total Time: 2.3457
Result: VIOLATED
Plotting..
 
'06_SinglePendulum' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Double Pendulum (less robust)
Time to compute random simulations: 0.68132
Time to check violation in simulations: 0.053727
Time to compute reachable set: 1.764
Time to check verification: 0.011123
Total Time: 2.5102
Result: VIOLATED
Plotting..
 
'07_DoublePendulum_lessRobust' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Double Pendulum (more robust)
Time to compute random simulations: 0.60346
Time to check violation in simulations: 0.025984
Time to compute reachable set: 1.1136
Time to check verification: 0.0078488
Total Time: 1.7509
Result: VIOLATED
Plotting..
 
'08_DoublePendulum_moreRobust' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Airplane
Time to compute random simulations: 0.71243
Time to check violation in simulations: 0.043433
Time to compute reachable set: 1.463
Time to check verification: 0.0085315
Total Time: 2.2274
Result: VIOLATED
Plotting..
 
'09_Airplane' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: middle)
dot{h}_0(0) = -19.5
Time to compute random simulations: 0.048986
Time to check violation in simulations: 0.019807
Time to compute reachable set: 0.11063
Time to check verification: 0.003348
Total Time: 0.18277
Result: VERIFIED
Plotting..
 
'10_VCAS_middle-19.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: middle)
dot{h}_0(0) = -22.5
Time to compute random simulations: 0.045093
Time to check violation in simulations: 0.024157
Time to compute reachable set: 0.059591
Time to check verification: 0.0001375
Total Time: 0.12898
Result: VERIFIED
Plotting..
 
'11_VCAS_middle-22.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: middle)
dot{h}_0(0) = -25.5
Time to compute random simulations: 0.025737
Time to check violation in simulations: 0.0007872
Total Time: 0.026524
Result: VIOLATED
Plotting..
 
'12_VCAS_middle-25.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: middle)
dot{h}_0(0) = -28.5
Time to compute random simulations: 0.028416
Time to check violation in simulations: 0.0004802
Total Time: 0.028896
Result: VIOLATED
Plotting..
 
'13_VCAS_middle-28.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: worst)
dot{h}_0(0) = -19.5
Time to compute random simulations: 0.042627
Time to check violation in simulations: 0.026347
Time to compute reachable set: 0.07661
Time to check verification: 0.000326
Total Time: 0.14591
Result: VERIFIED
Plotting..
 
'14_VCAS_worst-19.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: worst)
dot{h}_0(0) = -22.5
Time to compute random simulations: 0.042916
Time to check violation in simulations: 0.0068867
Total Time: 0.049802
Result: VIOLATED
Plotting..
 
'15_VCAS_worst-22.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: worst)
dot{h}_0(0) = -25.5
Time to compute random simulations: 0.023775
Time to check violation in simulations: 0.0006062
Total Time: 0.024382
Result: VIOLATED
Plotting..
 
'16_VCAS_worst-25.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Vertical Collision Avoidance System (VCAS: worst)
dot{h}_0(0) = -28.5
Time to compute random simulations: 0.023478
Time to check violation in simulations: 0.0004206
Total Time: 0.023899
Result: VIOLATED
Plotting..
 
'17_VCAS_worst-28.5' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Attitude Control
Time to compute random simulations: 0.63467
Time to check violation in simulations: 0.96644
Time to compute reachable set: 1.6115
Time to check verification: 0.033291
Total Time: 3.2459
Result: VERIFIED
Plotting..
 
'18_AttitudeControl' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Quadrotor (QUAD)
Refined layer 3 from order 1 to 2!
Time to compute random simulations: 1.3528
Time to check violation in simulations: 1.3825
Time to compute reachable set: 285.1146
Time to check verification: 0.12676
Total Time: 287.9767
Result: VERIFIED
Plotting..
 
'19_QUAD' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
BENCHMARK: Spacecraft Docking
Time to compute random simulations: 12.2417
Time to check violation in simulations: 0.067591
Time to compute reachable set: 29.1409
Time to check verification: 1.6716
Total Time: 43.1218
Result: UNKOWN
Plotting..
 
'20_SpacecraftDocking' was run successfully!
Saving plots to './results/plots'..
 
--------------------------------------------------------
 
Completed!
