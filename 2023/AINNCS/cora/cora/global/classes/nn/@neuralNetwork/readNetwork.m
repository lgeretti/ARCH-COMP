function obj = readNetwork(file_path)
% readNetwork - reads and converts a network according to file ending
%
% Syntax:
%    res = neuralNetwork.readNetwork(file_path)
%
% Inputs:
%    file_path: path to file
%
% Outputs:
%    obj - generated object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Tobias Ladner
% Written:      30-March-2022
% Last update:  30-November-2022 (inputArgsCheck)
% Last revision:---

%------------- BEGIN CODE --------------

% validate input
if ~isfile(file_path)
    throw(CORAerror('CORA:fileNotFound', file_path));
end
[~, ~, ext] = fileparts(file_path);
possibleExtensions = {'.onnx', '.nnet', '.yml', '.sherlock'};
inputArgsCheck(ext, 'str', possibleExtensions);

% redirect to specific read function
if strcmp(ext, '.onnx')
    obj = neuralNetwork.readONNXNetwork(file_path);
elseif strcmp(ext, '.nnet')
    obj = neuralNetwork.readNNetNetwork(file_path);
elseif strcmp(ext, '.yml')
    obj = neuralNetwork.readYMLNetwork(file_path);
elseif strcmp(ext, '.sherlock')
    obj = neuralNetwork.readSherlockNetwork(file_path);
else
    throw(CORAerror('CORA:wrongValue','first',...
        sprintf('has to end in %s', strjoin(possibleExtensions, ', ')) ...
    ));
end

%------------- END OF CODE --------------