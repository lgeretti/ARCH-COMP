function [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotope(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
% evaluatePolyZonotope - evaluates the activation layer on a polyZonotope
%
% Syntax:
%    [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotope(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
%
% Inputs:
%    c, G, Grest, expMat, id, id_, ind, ind_ - parameters of polyZonotope
%    evParams - parameter for NN evaluation
%
% Outputs:
%    updated [c, G, Grest, expMat, id, id_, ind, ind_]
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: nnActivationLayer/evaluatePolyZonotopeNeuron

% Author:        Tobias Ladner
% Written:       28-March-2022
% Last update:   05-April-2022
%                23-June-2022 (performance optimizations)
%                05-December-2022 (readability through aux functions)
%                16-February-2023 (combined approx_type)
%                21-March-2023 (bugfix Grest)
% Last revision: ---

%------------- BEGIN CODE --------------

% init bounds
num_neurons = size(G, 1);
if ~all(size(obj.l) == [num_neurons, 1])
    obj.l = nan(num_neurons, 1);
end
if ~all(size(obj.u) == [num_neurons, 1])
    obj.u = nan(num_neurons, 1);
end

% prepare properties for refinement
if ~all(size(obj.order) == size(c))
    obj.order = ones(size(c)) .* max(obj.order);
end
obj.do_refinement = ones(size(c));

% init
maxOrder = max(obj.order);

% order reduction prior to the evaluation
[c, G, Grest, expMat, id, id_, ind, ind_] = aux_preOrderReduction(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams);

if evParams.sort_exponents
    % sort columns of expMat
    [G, expMat] = aux_sort(obj, G, expMat, maxOrder);
end

% compute output sizes per order (improves performance)
[G_start, G_end, G_ext_start, G_ext_end] = nnHelper.getOrderIndicesG(G, maxOrder);
[~, Grest_end, ~, ~] = nnHelper.getOrderIndicesGrest(Grest, G, maxOrder);

% init
c_out = zeros(num_neurons, 1);
G_out = zeros(num_neurons, G_end(end));
Grest_out = zeros(num_neurons, Grest_end(end));
E_out = aux_computeE_out(obj, expMat, maxOrder, G_start, G_end);
d = zeros(num_neurons, 1);

% loop over all neurons in the current layer
for j = 1:num_neurons
    evParams.j = j;
    order_j = obj.order(j);

    [c_out(j), G_out_j, Grest_out_j, d(j)] = ...
        obj.evaluatePolyZonotopeNeuron( ...
        c(j), G(j, :), Grest(j, :), expMat, E_out, order_j, ...
        ind, ind_, evParams ...
        );

    G_out(j, 1:length(G_out_j)) = G_out_j;
    Grest_out(j, 1:length(Grest_out_j)) = Grest_out_j;
end

if evParams.sort_exponents
    % make sure columns of E_out remain sorted
    [G_out, E_out] = aux_sortPost(obj, G_out, E_out, maxOrder, ...
        G_start, G_end, G_ext_start, G_ext_end);
end

% update output
c = c_out;
G = G_out;
Grest = Grest_out;
expMat = E_out;

% order reduction post to the evaluation
[c, G, Grest, expMat, id, d, id_] = aux_postOrderReduction(obj, c, G, Grest, expMat, id, id_, d, evParams);

% add approximation error
[G, Grest, expMat, id, id_] = aux_addApproxError(obj, d, G, Grest, expMat, id, id_, evParams);

% update indices of all-even exponents (for zonotope encl.)
ind = find(prod(ones(size(expMat))-mod(expMat, 2), 1) == 1);
ind_ = setdiff(1:size(expMat, 2), ind);

end

% Auxiliary functions -----------------------------------------------------

function [c, G, Grest, expMat, id, id_, ind, ind_] = aux_preOrderReduction(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
% order reduction prior to the evaluation

% for image inputs the input dimension is very large, so we
% perform an order reduction here to reduce comp. time
if ~isempty(evParams.num_generators) && evParams.i == 2 && ...
        size(G, 2) + size(Grest, 2) > evParams.num_generators
    [c, G, Grest, expMat, id] = nnHelper.initialOrderReduction(c, ...
        G, Grest, expMat, id, id_, evParams.num_generators);
    id_ = max(max(id), id_);

    ind = find(prod(ones(size(expMat))- ...
        mod(expMat, 2), 1) == 1);
    ind_ = setdiff(1:size(expMat, 2), ind);
end

max_order = max(obj.order);

% pre order reduction
if evParams.do_pre_order_reduction && ~isempty(evParams.max_gens_post)
    h = size(G, 2);
    q = size(Grest, 2);
    num_gens = nthroot(evParams.max_gens_post, max_order);
    if h + q > num_gens
        % order reduction
        [c, G, Grest, expMat, id] = nnHelper.initialOrderReduction(c, ...
            G, Grest, expMat, id, id_, num_gens);
        id_ = max(max(id), id_);
        ind = find(prod(ones(size(expMat))-mod(expMat, 2), 1) == 1);
        ind_ = setdiff(1:size(expMat, 2), ind);
    end
end

% restructure pZ s.t. there remain no independent generators
if size(Grest, 2) > 0 && evParams.remove_Grest && ~isempty(evParams.max_gens_post)
    num_gens = nthroot(evParams.max_gens_post, max_order);
    num_neurons = size(G, 1);
    num_gens = max(num_neurons, num_gens);
    [c, G, Grest, expMat, id] = nnHelper.restructurePolyZono(c, G, Grest, expMat, id, id_, num_gens);
    id_ = max(max(id), id_);
    ind = find(prod(ones(size(expMat))- ...
        mod(expMat, 2), 1) == 1);
    ind_ = setdiff(1:size(expMat, 2), ind);

    if ~isempty(Grest)
        throw(CORAerror('CORA:specialError', 'Grest is not empty.'))
    end
    Grest = zeros(num_neurons, 0);
end
end

function [G, E] = aux_sort(obj, G, E, maxOrder)
    % sort columns of expMat in lexigraphic ordering
    if maxOrder > 1
        [E, idx] = sortrows(E');
        E = E';
        G = G(:, idx);
    end
end
 
function [G, E] = aux_sortPost(obj, G, E, maxOrder, G_start, G_end, G_ext_start, G_ext_end)
    % sort columns of expMat in lexigraphic ordering
    if maxOrder > 1
        % sort within PZ^i
        for i=2:maxOrder % skip PZ^1, already sorted
            G_i = G(:, G_start(i):G_end(i));
            E_i = E(:, G_start(i):G_end(i));


            i1 = floor(i/2);
            i2 = ceil(i/2);
            i1len = G_ext_end(i1)-G_ext_start(i1)+1;
            i2len = G_ext_end(i2)-G_ext_start(i2)+1;

            Gs_i = cell(1, i1);
            Es_i = cell(1, i1);
            if i1 == i2
                cnt = 1;
                for j = i1len:-1:1
                    Gs_i{j} = G_i(:, cnt:cnt+j-1);
                    Es_i{j} = E_i(:, cnt:cnt+j-1);
                    cnt = cnt + j;
                end
            else
                idx = num2cell(1:i1len);
                Gs_i = cellfun(@(i) G_i(:, (i-1)*i2len+(1:i2len)), ...
                    idx, 'UniformOutput', false);
                Es_i = cellfun(@(i) E_i(:, (i-1)*i2len+(1:i2len)), ...
                    idx, 'UniformOutput', false);
            end

            [G_i, E_i] = aux_sortPlus(obj, Gs_i, Es_i, size(G_i, 2));
            h_i = size(G_i, 2); % new number of generators
            G(:, G_start(i)-1+(1:h_i)) = G_i;
            G(:, G_start(i)+h_i:G_end(i)) = 0; % set unused to 0
            E(:, G_start(i)-1+(1:h_i)) = E_i;
        end

        % sort between PZ^i
        idx = num2cell(1:maxOrder);
        Gs = cellfun(@(i) G(:, G_start(i):G_end(i)), ...
            idx, 'UniformOutput', false);
        Es = cellfun(@(i) E(:, G_start(i):G_end(i)), ...
            idx, 'UniformOutput', false);
        [G, E] = aux_sortPlus(obj, Gs, Es, size(G, 2));
    end
end
 
function [G, E] = aux_sortPlus(obj, Gs, Es, h)
    % sorts a list of sorted expMats
    if isempty(Gs)
        G = []; E = [];
        return
    end

    % init heap
    heapInit = cell(1, length(Es));
    for i=1:length(Es)
        minObj = struct;
        minObj.idx = 1;
        minObj.i = i;
        minObj.key = Es{minObj.i}(:, minObj.idx)'; % key is row vector
        heapInit{i} = minObj;
    end

    G = zeros(size(Gs{1}, 1), h);
    E = zeros(size(Es{1}, 1), h);
    idx = 1;

    heapObj = nnHelper.heap(heapInit);
    while ~isempty(heapObj)
        minObj = min(heapObj);
        G_i = Gs{minObj.i};
        G(:, idx) = G_i(:, minObj.idx);
        E(:, idx) = minObj.key';
        idx = idx+1;

        if(minObj.idx+1 <= size(G_i, 2))
            % check if last entry of current min Gs/Es is smaller than
            % current min of heap
            E_i = Es{minObj.i};
            minObjNext = min(heapObj);
            [~, ids] = sortrows([minObjNext.key; E_i(:, end)']);
            if ids(1) == 1
                % add next generator
                minObjNew = struct;
                minObjNew.idx = minObj.idx+1;
                minObjNew.i = minObj.i;
                minObjNew.key = Es{minObjNew.i}(:, minObjNew.idx)';
                replaceMin(heapObj, minObjNew);
            else
                % add all remaining generators
                remGens = size(G_i, 2)-minObj.idx;
                G(:, idx:idx+remGens-1) = G_i(:, end-remGens+1:end);
                E(:, idx:idx+remGens-1) = E_i(:, end-remGens+1:end);
                idx = idx + remGens;
                pop(heapObj);
            end
        else
            % remove current min
            pop(heapObj);
        end
    end
            
    [G, E] = aux_merge(obj, G, E);
end

function [G, E] = aux_merge(obj, G, E)
    % merge a sorted, non-regular exponent matrix
    i_out = 1;
    h = size(G, 2);

    for i=2:h
        if E(:, i_out) == E(:, i)
            G(:, i_out) = G(:, i_out) + G(:, i);
        else
            i_out = i_out + 1;
            if i_out ~= i
                G(:, i_out) = G(:, i);
                E(:, i_out) = E(:, i);
            end
        end
    end

    G(:, i_out+1:end) = [];
    E(:, i_out+1:end) = [];
end

function E_out = aux_computeE_out(obj, expMat, order, G_start, G_end)
% comput output exponential matrix

% init
E_out = zeros(size(expMat, 1), G_end(end));
E_ext = cell(1, order);
E_out(:, 1:G_end(1)) = expMat;
E_ext{1} = expMat;

for i = 2:order
    % Note that e.g., G2 * G3 = G5 -> E2 + E3 = E5
    i1 = floor(i/2);
    i2 = ceil(i/2);

    Ei1_ext = E_ext{i1};
    Ei2_ext = E_ext{i2};
    Ei = nnHelper.calcSquaredE(Ei1_ext, Ei2_ext, i1 == i2);
    Ei_ext = [Ei1_ext, Ei2_ext, Ei];

    if i1 < i2
        % free memory
        E_ext{i1} = [];
    end

    E_out(:, G_start(i):G_end(i)) = Ei;
    E_ext{i} = Ei_ext;
end
end

function [c, G, Grest, expMat, id, d, id_] = aux_postOrderReduction(obj, c, G, Grest, expMat, id, id_, d, evParams)
% order reduction post to the evaluation

if ~isempty(evParams.num_generators) && evParams.num_generators < size(G, 2) + size(Grest, 2)
    % order reduction
    [c, G, Grest, expMat, id, d] = nnHelper.reducePolyZono(c, G, Grest, ...
        expMat, id, d, evParams.num_generators);
    id_ = max(max(id), id_);
elseif max(obj.order) > 1 && ~evParams.sort_exponents
    % exponents remain equal with order = 1
    [expMat, G] = removeRedundantExponents(expMat, G);
end
end


function [G, Grest, expMat, id, id_] = aux_addApproxError(obj, d, G, Grest, expMat, id, id_, evParams)
% process approximation error

% save error bound for refinement
obj.refine_heu = d ...
    .* obj.do_refinement; % exclude neurons which should not be refined

temp = diag(d);
temp = temp(:, d > 0);
if evParams.add_approx_error_to_Grest
    Grest = [Grest, temp];
else
    G = [G, temp];
    expMat = blkdiag(expMat, eye(size(temp, 2)));
    id = [id; (1:size(temp, 2))' + id_];
    id_ = max(id);
end
end

%------------- END OF CODE --------------
