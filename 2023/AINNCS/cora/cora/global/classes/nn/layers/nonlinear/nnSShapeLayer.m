classdef (Abstract) nnSShapeLayer < nnActivationLayer
% nnSShapeLayer - abstract class for S-shape like layers (sigmoid, tanh)
%
% Syntax:
%    abstract class
%
% Inputs:
%    name - name of the layer, defaults to type
%
% Outputs:
%    obj - generated object
%
% References:
%    [1] Singh, G., et al. "Fast and Effective Robustness Certification"
%    [2] R. Ivanov, et al. "Verisig 2.0: Verification of Neural Network
%        Controllers Using Taylor Model Preconditioning", 2021
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: neuralNetwork

% Author:       Tobias Ladner
% Written:      28-March-2022
% Last update:  16-February-2023 (combined approx_type)
% Last revision:10-August-2022 (renamed)

%------------- BEGIN CODE --------------

properties (Constant)
end

properties
    sym_dfs = cell(0, 1)
end

methods
    % constructor
    function obj = nnSShapeLayer(name)
        % call super class constructor
        obj@nnActivationLayer(name)
    end

    function df_i = getDf(obj, i)
        if i == 0
            df_i = obj.f;
        elseif i <= size(obj.dfs, 1)
            df_i = obj.dfs{i};
        elseif i <= size(obj.sym_dfs, 1)
            df_i = matlabFunction(obj.sym_dfs{i});
        else
            syms x real
            if i == 1
                obj.sym_dfs{1, 1} = simplify(diff(obj.f(x)));
            end

            for j = 2:i
                obj.sym_dfs{j, 1} = simplify(diff(obj.sym_dfs{j-1, 1}));
            end

            df_i = matlabFunction(obj.sym_dfs{i});
        end
    end

    function der1 = getDerBounds(obj, l, u)
        df = obj.getDf(1);
        % df_l and df_u as lower and upper bound for the derivative
        % case distinction for all combinations of l and u
        if l <= 0 && u >= 0
            % global upper bound for the derivative of s-shaped functions
            df_u = df(0);
            % evaluate the lower bound in this case
            if abs(l) >= abs(u)
                df_l = df(l);
            else
                df_l = df(u);
            end
            % two leftover cases
        elseif l > 0 && u > 0
            df_l = df(u);
            df_u = df(l);
        elseif l < 0 && u < 0
            df_l = df(l);
            df_u = df(u);
        end
    
        % if l, u is very large, df is NaN
        if any(isnan([df_l, df_u]))
            df_l = 0;
            df_u = 0.0001;
        end
    
        % return interval
        der1 = interval(df_l, df_u);
    end
end

% evaluate ----------------------------------------------------------------

% implemented in sup-/subclasses

% Auxiliary function ------------------------------------------------------

methods (Access=protected)
    function [coeffs, d] = computeApproxPolyCustom(obj, l, u, order, poly_method)
        % implement custom polynomial computation in subclass
        coeffs = []; d = [];
        
        f = obj.f;
        df = obj.getDf(1);

        if strcmp(poly_method, 'hand-crafted')
            if order == 1
                % according to [1, Theorem 3.2]
                lambda = min(df(l), df(u));
                mu1 = 0.5 * (f(u) + f(l) - lambda * (u + l));
                mu2 = 0.5 * (f(u) - f(l) - lambda * (u - l));
                coeffs = [lambda, mu1];
                d = mu2;
            end

        elseif strcmp(poly_method, 'taylor')
            % taylor series expansion
            df2 = obj.getDf(2);
            df3 = obj.getDf(3);

            c = (l+u)/2;

            if order == 1
                % according to [1, Theorem 3.2]
                lambda = min(df(l), df(u));
                mu1 = 0.5 * (f(u) + f(l) - lambda * (u + l));
                mu2 = 0.5 * (f(u) - f(l) - lambda * (u - l));
                coeffs = [lambda, mu1];
                d = mu2;

            elseif order == 2
                % according to [2, Ex. 1]
                c_a = 0.5 * df2(c);
                c_b = df(c) - df2(c) * c;
                c_c = f(c) - df(c) * c + 0.5 * df2(c) * c^2;
                coeffs = [c_a, c_b, c_c];

            elseif order == 3
                c_a = 1 / 6 * df3(c);
                c_b = 0.5 * df2(c) - 0.5 * df3(c) * c;
                c_c = df(c) - df2(c) * c + 0.5 * c^2 * df3(c);
                c_d = f(c) - df(c) * c + 0.5 * df2(c) * c^2 - 1 / 6 * df3(c) * c^3;
                coeffs = [c_a, c_b, c_c, c_d];

            end
        
        elseif strcmp(poly_method, "throw-catch")
            coeffs = nnHelper.calcAlternatingDerCoeffs(l, u, order, obj);
            
        end
    end
end

end

%------------- END OF CODE --------------