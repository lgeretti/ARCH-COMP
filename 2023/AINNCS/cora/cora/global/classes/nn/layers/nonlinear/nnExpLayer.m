classdef nnExpLayer < nnSShapeLayer
% nnExpLayer - class for exp layers
%
% Syntax:
%    obj = nnExpLayer(name)
%
% Inputs:
%    name - name of the layer, defaults to type
%
% Outputs:
%    obj - generated object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: NeuralNetwork

% Author:       Tobias Ladner
% Written:      19-July-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

methods
    % constructor
    function obj = nnExpLayer(name)
        if nargin < 2
            name = [];
        end
        % call super class constructor
        obj@nnSShapeLayer(name)
    end

    function der1 = getDerBounds(obj, l, u)
        % df_l and df_u as lower and upper bound for the derivative
        der1 = interval(exp(l), exp(u));
    end
end

% evaluate ----------------------------------------------------------------

methods  (Access = {?nnLayer, ?neuralNetwork})
    % numeric
    function [r, obj] = evaluateNumeric(obj, input, evParams)
        r = exp(input);
    end
end

end

%------------- END OF CODE --------------
