from src.actors.agents.agent import Agent
from src.verification.bounds.bounds import HyperRectangleBounds

import numpy as np


class UnicycleAgent(Agent):

    def __init__(self, controller_model):
        """
        """
        # The dimensionality of the action space,
        # it is 1 as the action is the advisory.
        self.ACTION_SPACE_DIM = 1

        # the networks themselves
        self.controller_model = controller_model

        self.offset = 20
        self.scaling_factor = 1

        super(UnicycleAgent, self).__init__()

    def get_constraints_for_action(self, constrs_manager, input_state_vars):
        """
        Create constraints for performing an action. Constraints are only added to the model by
        the caller, to reduce side-effects in this function.
        :param constrs_manager: Manager of constraints.
        :param input_state_vars: variables representing the input state passed to the agent.
        :return: Singleton list of variables.
        :side-effects: Modifies constraints manager when adding variables.
        """

        constrs_to_add = []

        u_vars, network_constrs = constrs_manager.get_network_constraints(
            self.controller_model.layers, input_state_vars)

        u_bounds = constrs_manager.get_variable_bounds(u_vars)

        # transformed_u = (u - offset) * scaling_factor
        trans_u_lower = (np.array(u_bounds.get_lower()) - self.offset * np.ones(len(u_vars))) * self.scaling_factor
        trans_u_upper = (np.array(u_bounds.get_upper()) - self.offset * np.ones(len(u_vars))) * self.scaling_factor

        transformed_u = constrs_manager.create_state_variables(len(u_vars), lbs=trans_u_lower, ubs=trans_u_upper)
        constrs_manager.add_variable_bounds(transformed_u,
                                            HyperRectangleBounds(trans_u_lower, trans_u_upper))

        constrs_to_add.extend(network_constrs)

        # transformed_u = (u - offset) * scaling_factor
        for i in range(len(u_vars)):
            constrs_to_add.append(
                constrs_manager.get_linear_constraint([transformed_u[i], u_vars[i]],
                                                      [1, -self.scaling_factor],
                                                      -self.offset * self.scaling_factor)
            )

        return transformed_u, constrs_to_add

