function check_pHA_inputs(options, obj)
% check_flatHA_inputs - checks if inputs
%  (UCompLoc, uCompLocTrans, UGlob, uGlobTrans, inputCompMap)
%  1) exist
%  2) take an allowed value
%
% Syntax:
%    check_pHA_inputs(options, obj)
%
% Inputs:
%    options - options for object
%    obj     - parallel hybrid automaton object
%
% Outputs:
%    -
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% References: 
%   -

% Author:       Mark Wetzlinger
% Written:      13-Mar-2019
% Last update:  03-May-2020 (rewriting of error msgs using class(obj))
% Last revision:---

%------------- BEGIN CODE --------------

strct = 'params';

numComp = length(obj.components);

% check existence and emptyness
opts = {'UCompLoc';'uCompLocTrans';'UGlob';'uGlobTrans';'inputCompMap'};
for i=1:length(opts)
    if ~isfield(options,opts{i})
        error(printOptionMissing(obj,opts{i},strct));
    end
    if isempty(options.(opts{i}))
        error(printOptionOutOfRange(obj,opts{i},strct));
    end
end

% UCompLoc, uCompLocTrans for each component
if length(options.UCompLoc) ~= numComp
    error(printOptionOutOfRange(obj,'UCompLoc',strct));
elseif length(options.uCompLocTrans) ~= numComp
    error(printOptionOutOfRange(obj,'uCompLocTrans',strct));
end

% inputCompMap maps from inputBinds which are [0,x], find nr. of zeros
iB = obj.bindsInputs;
len_inputCompMap = 0;
for i=1:length(iB)
    if iB{i}(1) == 0
        len_inputCompMap = max(len_inputCompMap,iB{i}(2));
    end
end
if length(options.inputCompMap) ~= len_inputCompMap
    error(printOptionOutOfRange(obj,'inputCompMap',strct));
end

% UGlob, uGlobTrans is number of zeros in inputCompMap
len_UGlob = 0;
if ~isempty(options.inputCompMap)
    len_UGlob = length(options.inputCompMap) - nnz(options.inputCompMap);
end
if length(options.UGlob) ~= len_UGlob
    error(printOptionOutOfRange(obj,'UGlob',strct));
elseif length(options.uGlobTrans) ~= len_UGlob
    error(printOptionOutOfRange(obj,'uGlobTrans',strct));
end


end

%------------- END OF CODE --------------

