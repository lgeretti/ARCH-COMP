function han = plot(obj,varargin)
% plot - Plots 2-dimensional projection of a constrained hyperplane
%
% Syntax:  
%    plot(obj)
%    plot(obj,dim,color)
%
% Inputs:
%    obj - halfspace object
%    dimensions - dimensions that should be projected (optional); assume
%                 that other entries of the normal vector are zeros
%    color - color ('r', 'b', etc.)
%    
%
% Outputs:
%    han - handle to the plotted object
%
% Example: 
%    C = [-1 -1; 1 0;-1 0; 0 1; 0 -1];
%    d = [2;3;2;3;2];
%    cH = conHyperplane(halfspace([1;1],2),C,d);
%
%    figure
%    hold on
%    plot(mptPolytope(C,d),[1,2],'g');
%    xlim([-4,4]);
%    ylim([-4,4]);
%    plot(cH,[1,2],'r');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: halfspace/plot

% Author:       Niklas Kochdumper
% Written:      19-November-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % parse input arguments
    dim = [1,2];
    color = 'b';

    if nargin >= 2 && ~isempty(varargin{1})
        dim = varargin{1};
    end

    if nargin >= 3 && ~isempty(varargin{2})
        color = varargin{2}; 
    end

    % get size of current plot
    xLim = get(gca,'Xlim');
    yLim = get(gca,'Ylim');

    % compute vertices
    if ~isempty(obj.C)
        C = obj.C(:,dim);
    else
        C = []; 
    end
    d = obj.d;

    C = [C;eye(2);-eye(2)];
    d = [d;xLim(2);yLim(2);-xLim(1);-yLim(1)];

    vert = lcon2vert(C,d,obj.h.c(dim)',obj.h.d);

    % plot constrained hyperplane
    han = plot([vert(1,1),vert(2,1)],[vert(1,2),vert(2,2)],color,varargin{3:end});

%------------- END OF CODE --------------