function f = prodDesParam(x,u)
% differential equation for the production-destruction system from the ARCH
% competition

    f(1,1) = (-x(1)*x(2))/(1+x(1));
    f(2,1) = (x(1)*x(2))/(1+x(1)) - x(4) * x(2);
    f(3,1) = x(4) * x(2);
    f(4,1) = 0;

end