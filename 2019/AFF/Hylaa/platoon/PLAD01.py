'''
Platoon System for ARCHCOMP'19

This is the deterministic switching case (PLAD01).

modified version from original created Hyst v1.6 which converted a model prepared by Stefan Schupp (thanks)
'''


import sys
import numpy as np

from hylaa.hybrid_automaton import HybridAutomaton
from hylaa.settings import HylaaSettings, PlotSettings
from hylaa.core import Core
from hylaa.stateset import StateSet
from hylaa import lputil, aggstrat

def define_ha(bound):
    '''make the hybrid automaton and return it'''

    epsilon = 1e-6

    c1 = 5 - epsilon # switch from comm -> lost
    c2 = 5 - epsilon # switch from lost -> comm

    ha = HybridAutomaton()

    # dynamics variable order: [e1, v1, a1, e2, v2, a2, e3, v3, a3, t, affine]
    # input variable order: [aL]

    comm = ha.new_mode('comm')
    a_matrix = [ \
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0], \
        [1.605, 4.868, -3.5754, -0.8198, 0.427, -0.045, -0.1942, 0.3626, -0.0946, 0, 0], \
        [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0], \
        [0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0], \
        [0.8718, 3.814, -0.0754, 1.1936, 3.6258, -3.2396, -0.595, 0.1294, -0.0796, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0], \
        [0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0], \
        [0.7132, 3.573, -0.0964, 0.8472, 3.2568, -0.0876, 1.2726, 3.072, -3.1356, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        ]
    comm.set_dynamics(a_matrix)
    
    # -9.0 <= aL
    # aL <= 1.0
    u_constraints_a = np.array([[-1], [1]], dtype=float)
    u_constraints_b = np.array([9, 1], dtype=float)
    b_matrix = np.array([[0], [1], [0], [0], [0], [0], [0], [0], [0], [0], [0]], dtype=float)
    comm.set_inputs(b_matrix, u_constraints_a, u_constraints_b)
    # t <= c1
    comm.set_invariant([[0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0]], [c1])

    lost = ha.new_mode('lost')
    a_matrix = [ \
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0], \
        [1.605, 4.868, -3.5754, 0, 0, 0, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0], \
        [0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0], \
        [0, 0, 0, 1.1936, 3.6258, -3.2396, 0, 0, 0, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0], \
        [0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0], \
        [0.7132, 3.573, -0.0964, 0.8472, 3.2568, -0.0876, 1.2726, 3.072, -3.1356, 0, 0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        ]
    lost.set_dynamics(a_matrix)
    
    # -9.0 <= aL
    # aL <= 1.0
    u_constraints_a = np.array([[-1], [1]], dtype=float)
    u_constraints_b = np.array([9, 1], dtype=float)
    b_matrix = np.array([[0], [1], [0], [0], [0], [0], [0], [0], [0], [0], [0]], dtype=float)
    lost.set_inputs(b_matrix, u_constraints_a, u_constraints_b,)
    # t <= c2
    lost.set_invariant([[0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0]], [c2])

    trans = ha.new_transition(comm, lost)
    # t >= c1
    trans.set_guard([[-0, -0, -0, -0, -0, -0, -0, -0, -0, -1, -0], ], [-c1, ])
    
    # Reset:
    # t := 0.0
    reset_clock_mat = [ \
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ], \
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, ], \
        [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, ], \
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, ], \
        [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, ], \
        [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, ], \
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, ], \
        [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, ], \
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, ], \
        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, ], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, ], \
        ]
    trans.set_reset(reset_clock_mat)

    trans = ha.new_transition(lost, comm, 'restoring')

    # t >= c2
    trans.set_guard([[-0, -0, -0, -0, -0, -0, -0, -0, -0, -1, -0], ], [-c2, ])
    
    # Reset:
    # t := 0.0
    trans.set_reset(reset_clock_mat)

    # add transitions to error mode
    if bound is not None:
        dims = len(a_matrix)
        error = ha.new_mode('error')

        # error if e1, e2, e3 <= -bound in either mode
        for e_var in [0, 3, 6]:
            trans = ha.new_transition(comm, error)
            trans.set_guard([[1 if n == e_var else 0 for n in range(dims)]], [-bound])

            trans = ha.new_transition(lost, error)
            trans.set_guard([[1 if n == e_var else 0 for n in range(dims)]], [-bound])

    return ha

def define_init_states(ha):
    '''returns a list of StateSet objects'''
    # Variable ordering: [e1, v1, a1, e2, v2, a2, e3, v3, a3, t, affine]
    rv = []
    
    # t = 0.0 & e1 = 0.0 & v1 = 0.0 & a1 = 0.0 & e2 = 0.0 & v2 = 0.0 & a2 = 0.0 & e3 = 0.0 & v3 = 0.0 & a3 = 0.0
    # & affine = 1.0
    mode = ha.modes['comm']
    mat = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0], \
        [-0, -0, -0, -0, -0, -0, -0, -0, -0, -1, -0], \
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [-1, -0, -0, -0, -0, -0, -0, -0, -0, -0, -0], \
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0], \
        [-0, -1, -0, -0, -0, -0, -0, -0, -0, -0, -0], \
        [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0], \
        [-0, -0, -1, -0, -0, -0, -0, -0, -0, -0, -0], \
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0], \
        [-0, -0, -0, -1, -0, -0, -0, -0, -0, -0, -0], \
        [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0], \
        [-0, -0, -0, -0, -1, -0, -0, -0, -0, -0, -0], \
        [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0], \
        [-0, -0, -0, -0, -0, -1, -0, -0, -0, -0, -0], \
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0], \
        [-0, -0, -0, -0, -0, -0, -1, -0, -0, -0, -0], \
        [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0], \
        [-0, -0, -0, -0, -0, -0, -0, -1, -0, -0, -0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0], \
        [-0, -0, -0, -0, -0, -0, -0, -0, -1, -0, -0], \
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], \
        [-0, -0, -0, -0, -0, -0, -0, -0, -0, -0, -1], ]
    rhs = [0, -0, 0, -0, 0, -0, 0, -0, 0, -0, 0, -0, 0, -0, 0, -0, 0, -0, 0, -0, 1, -1, ]
    rv.append(StateSet(lputil.from_constraints(mat, rhs, mode), mode))
    
    return rv


def define_settings(image_path):
    '''get the hylaa settings object
    see hylaa/settings.py for a complete list of reachability settings'''

    settings = HylaaSettings(0.1, 20.0)

    if image_path is not None:
        settings.plot.plot_mode = PlotSettings.PLOT_IMAGE

        settings.plot.filename = 'PLAD01'
        settings.plot.xdim_dir = None
        settings.plot.ydim_dir = 0

        settings.plot.plot_size = (6, 6)
        settings.plot.label.title = ''
        settings.plot.label.y_label = '$e_1$'
        settings.plot.label.x_label = '$t$'

        settings.plot.label.axes_limits = (0, 20, -30, 5) # [0,20] x [-30,5]

        settings.plot.use_markers_for_small = False

    return settings


def run_hylaa():
    'runs hylaa, returning a HylaaResult object'

    bound = None
    image_path = None

    if len(sys.argv) > 1 and sys.argv[1] == 'BND42':
        bound = 42
    elif len(sys.argv) > 1 and sys.argv[1] == 'BND30':
        bound = 30
    elif len(sys.argv) > 1 and sys.argv[1] == 'BND50':
        bound = 50
    elif len(sys.argv) > 1 and sys.argv[1] == 'plot':
        image_path = 'PLAD01.png'
    else:
        print("Expected single argument: BND50, BND42, BND30, or plot")
        sys.exit(1)

    ha = define_ha(bound)
    init = define_init_states(ha)
    settings = define_settings(image_path)
    result = Core(ha, settings).run(init)

    assert not result.has_concrete_error, "Incorrect result, system should be safe"


if __name__ == '__main__':
    run_hylaa()

 
