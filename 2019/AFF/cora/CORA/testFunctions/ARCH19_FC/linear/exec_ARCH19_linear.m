function exec_ARCH19_linear(path)

    % create path where the results are saved
    path = fullfile(pwd,path);
    
    % gearbox benchmark
    example_hybrid_reach_ARCH19_gearbox_GRBX01();
    print(fullfile(path,'gearbox_GRBX01.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_gearbox_GRBX02();
    print(fullfile(path,'gearbox_GRBX02.png'),'-dpng');
    close all
    
    % drivetrain benchmark
    example_hybrid_reach_ARCH19_powerTrain_DTN01();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'drivetrain_DTN01_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'drivetrain_DTN01_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_powerTrain_DTN02();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'drivetrain_DTN02_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'drivetrain_DTN02_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_powerTrain_DTN03();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'drivetrain_DTN03_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'drivetrain_DTN03_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_powerTrain_DTN04();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'drivetrain_DTN04_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'drivetrain_DTN04_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_powerTrain_DTN05();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'drivetrain_DTN05_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'drivetrain_DTN05_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_powerTrain_DTN06();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'drivetrain_DTN06_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'drivetrain_DTN06_2.png'),'-dpng');
    close all
    
    % spacecreaft rendezvous benchmark
    example_hybrid_reach_ARCH19_rendezvous_SRA01();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'rendezvous_SRA01_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'rendezvous_SRA01_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_rendezvous_SRA02();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'rendezvous_SRA02_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'rendezvous_SRA02_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_rendezvous_SRA03();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'rendezvous_SRA03_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'rendezvous_SRA03_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_rendezvous_SRA04();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'rendezvous_SRA04_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'rendezvous_SRA014_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_rendezvous_SRA05();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'rendezvous_SRA05_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'rendezvous_SRA05_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_rendezvous_SRA06();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'rendezvous_SRA06_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'rendezvous_SRA06_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_rendezvous_SRA07();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'rendezvous_SRA07_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'rendezvous_SRA07_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_rendezvous_SRA08();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'rendezvous_SRA08_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'rendezvous_SRA08_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_rendezvous_SRNA01();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'rendezvous_SRNA01_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'rendezvous_SRNA01_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_rendezvous_SRU01();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'rendezvous_SRU01_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'rendezvous_SRU01_2.png'),'-dpng');
    close all
    
    example_hybrid_reach_ARCH19_rendezvous_SRU02();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'rendezvous_SRU02_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'rendezvous_SRU02_2.png'),'-dpng');
    close all

    % building benchmark
    example_linear_reach_ARCH19_building_BLDC01();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'building_BLDC01_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'building_BLDC01_2.png'),'-dpng');
    close all
    
    example_linear_reach_ARCH19_building_BLDF01();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'building_BLDF01_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'building_BLDF01_2.png'),'-dpng');
    close all
    
    % iss benchmark
    example_linear_reach_ARCH19_iss_ISSC01_ISS02();
    print(fullfile(path,'iss_ISSC01_ISS02.png'),'-dpng');
    close all
    
    example_linear_reach_ARCH19_iss_ISSC01_ISU02();
    print(fullfile(path,'iss_ISSC01_ISU02.png'),'-dpng');
    close all
    
    example_linear_reach_ARCH19_iss_ISSF01_ISS01();
    print(fullfile(path,'iss_ISSF01_ISS01.png'),'-dpng');
    close all
    
    example_linear_reach_ARCH19_iss_ISSF01_ISU01();
    print(fullfile(path,'iss_ISSF01_ISU01.png'),'-dpng');
    close all
    
    % platooning benchmark
    example_linearParam_reach_ARCH19_platoon_PLAA01_BND42();
    print(fullfile(path,'platoon_PLAA01_BND42.png'),'-dpng');
    close all
    
    example_linearParam_reach_ARCH19_platoon_PLAA01_BND50();
    print(fullfile(path,'platoon_PLAA01_BND50.png'),'-dpng');
    close all
    
    example_linearParam_reach_ARCH19_platoon_PLAD01_BND30();
    print(fullfile(path,'platoon_PLAD01_BND30.png'),'-dpng');
    close all
    
    example_linearParam_reach_ARCH19_platoon_PLAD01_BND42();
    print(fullfile(path,'platoon_PLAD01_BND42.png'),'-dpng');
    close all
    
    example_linearParam_reach_ARCH19_platoon_PLAN01();
    print(fullfile(path,'platoon_PLAN01.png'),'-dpng');
    close all
    
end