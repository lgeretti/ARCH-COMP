Theorem "Static semantics correctness: Assignment 1"

ProgramVariables
  Real x;
End.

Problem
  x>=0->[x:=x+1;]x>=1
End.


End.

Theorem "Static semantics correctness: Assignment 2"

ProgramVariables
  Real x;
  Real y;
End.

Problem
  x>=0->[x:=x+1;][x:=x+1;++y:=x+1;]x>=1
End.


End.

Theorem "Static semantics correctness: Assignment 3"

ProgramVariables
  Real x;
End.

Problem
  x>=0->[x:=x+1;][{x:=x+1;}*]x>=1
End.


End.

Theorem "Static semantics correctness: Assignment 4"

ProgramVariables
  Real x;
End.

Problem
  x>=0->[x:=x+1;][{x'=2&true}]x>=1
End.


End.

Theorem "Static semantics correctness: Assignment 5"

ProgramVariables
  Real x;
End.

Problem
  x>=0->[x:=x+1;][x:=*;?x>=1;]x>=1
End.


End.

Theorem "Static semantics correctness: Assignment 6"

ProgramVariables
  Real x;
  Real y;
End.

Problem
  x>=0->[x:=x+1;][?x>=2;x:=x-1;++?\forall x (x>=1->y>=1);x:=y;]x>=1
End.


End.

Theorem "Static semantics correctness: Assignment 7"

ProgramVariables
  Real x;
  Real y;
End.

Problem
  x>=0&y>=1->[x:=x+1;][{x:=x+1;}*++y:=x+1;][{y'=2&true}][x:=y;]x>=1
End.


End.

Theorem "Static semantics correctness: Assignment 8"

ProgramVariables
  Real x;
  Real y;
End.

Problem
  x>0&y>0->[{x'=5&true}][{x:=x+3;}*++y:=x;](x>0&y>0)
End.


End.

Theorem "Static semantics correctness: Assignment 9"

ProgramVariables
  Real x;
  Real y;
End.

Problem
  x>0&y>0->[{x'=-x&true}][{x:=x+3;}*++y:=x;](x>0&y>0)
End.


End.

Theorem "Dynamics: Cascaded"

ProgramVariables
  Real x;
End.

Problem
  x>0->[{x'=5&true}{x'=2&true}{x'=x&true}]x>0
End.


End.

Theorem "Dynamics: Single integrator time"

ProgramVariables
  Real x;
End.

Problem
  x=0->[{x'=1&true}]x>=0
End.


End.

Theorem "Dynamics: Single integrator"

ProgramVariables
  Real x;
  Real y;
End.

Problem
  x>=0&y>=0->[{x'=y&true}]x>=0
End.


End.

Theorem "Dynamics: Double integrator"

ProgramVariables
  Real y;
  Real z;
  Real x;
End.

Problem
  x>=0&y>=0&z>=0->[{x'=y,y'=z&true}]x>=0
End.


End.

Theorem "Dynamics: Triple integrator"

ProgramVariables
  Real y;
  Real z;
  Real j;
  Real x;
End.

Problem
  x>=0&y>=0&z>=0&j>=0->[{x'=y,y'=z,z'=j,j'=j^2&true}]x>=0
End.


End.

Theorem "Dynamics: Exponential decay (1)"

ProgramVariables
  Real x;
End.

Problem
  x>0->[{x'=-x&true}]x>0
End.


End.

Theorem "Dynamics: Exponential decay (2)"

ProgramVariables
  Real x;
End.

Problem
  x>0->[{x'=-x+1&true}]x>0
End.


End.

Theorem "Dynamics: Exponential decay (3)"

ProgramVariables
  Real x;
  Real y;
End.

Problem
  x>0&y>0->[{x'=-y*x&true}]x>0
End.


End.

Theorem "Dynamics: Exponential growth (1)"

ProgramVariables
  Real x;
End.

Problem
  x>=0->[{x'=x&true}]x>=0
End.


End.

Theorem "Dynamics: Exponential growth (2)"

ProgramVariables
  Real x;
  Real y;
End.

Problem
  x>=0&y>=0->[{x'=y,y'=y^2&true}]x>=0
End.


End.

Theorem "Dynamics: Exponential growth (4)"

ProgramVariables
  Real x;
End.

Problem
  x>0->[{x'=x^x&true}]x>0
End.


End.

Theorem "Dynamics: Exponential growth (5)"

ProgramVariables
  Real x;
End.

Problem
  x>=1->[{x'=x^2+2*x^4&true}]x^3>=x^2
End.


End.

Theorem "Dynamics: Rotational dynamics (1)"

ProgramVariables
  Real x;
  Real y;
End.

Problem
  x^2+y^2=1->[{x'=-y,y'=x&true}]x^2+y^2=1
End.


End.

Theorem "Dynamics: Rotational dynamics (2)"

ProgramVariables
  Real y;
  Real e;
  Real x;
End.

Problem
  x^2+y^2=1&e=x->[{x'=-y,y'=e,e'=-y&true}](x^2+y^2=1&e=x)
End.


End.

Theorem "Dynamics: Rotational dynamics (3)"

ProgramVariables
  Real d1;
  Real x1;
  Real p;
  Real w;
  Real d2;
  Real x2;
End.

Problem
  d1^2+d2^2=w^2*p^2&d1=-w*x2&d2=w*x1->[{x1'=d1,x2'=d2,d1'=-w*d2,d2'=w*d1&true}](d1^2+d2^2=w^2*p^2&d1=-w*x2&d2=w*x1)
End.


End.

Theorem "Dynamics: Spiral to equilibrium"

ProgramVariables
  Real y;
  Real w;
  Real x;
End.

Problem
  w>=0&x=0&y=3->[{x'=y,y'=-w^2*x-2*w*y&true}]w^2*x^2+y^2<=9
End.


End.

Theorem "Dynamics: Open cases"

ProgramVariables
  Real x;
  Real y;
End.

Problem
  x^3>5&y>2->[{x'=x^3+x^4,y'=5*y+y^2&true}](x^3>5&y>2)
End.


End.

Theorem "Dynamics: Closed cases"

ProgramVariables
  Real y;
  Real z;
  Real x;
End.

Problem
  x>=1&y=10&z=-2->[{x'=y,y'=z+y^2-y&y>=0}](x>=1&y>=0)
End.


End.

Theorem "Dynamics: Conserved quantity"

ProgramVariables
  Real x1;
  Real c;
  Real x2;
End.

Problem
  x1^4*x2^2+x1^2*x2^4-3*x1^2*x2^2+1<=c->[{x1'=2*x1^4*x2+4*x1^2*x2^3-6*x1^2*x2,x2'=-4*x1^3*x2^2-2*x1*x2^4+6*x1*x2^2&true}]x1^4*x2^2+x1^2*x2^4-3*x1^2*x2^2+1<=c
End.


End.

Theorem "Dynamics: Darboux equality"
Definitions
  Real B();
End.

ProgramVariables
  Real z;
  Real A;
  Real x;
End.

Problem
  x+z=0->[{x'=A*x^2+B()*x,z'=A*z*x+B()*z&true}]0=-x-z
End.


End.

Theorem "Dynamics: Fractional Darboux equality"
Definitions
  Real B();
End.

ProgramVariables
  Real y;
  Real z;
  Real A;
  Real x;
End.

Problem
  x+z=0->[{x'=(A*y+B()*x)/z^2,z'=(A*x+B())/z&y=x^2&z^2>0}]x+z=0
End.


End.

Theorem "Dynamics: Darboux inequality"

ProgramVariables
  Real y;
  Real z;
  Real x;
End.

Problem
  x+z>=0->[{x'=x^2,z'=z*x+y&y=x^2}]x+z>=0
End.


End.

Theorem "Dynamics: Bifurcation"

ProgramVariables
  Real r;
  Real x;
  Real f;
End.

Problem
  r<=0->\exists f (x=f->[{x'=r+x^2&true}]x=f)
End.


End.

Theorem "Dynamics: Parametric switching between two different damped oscillators"

ProgramVariables
  Real d;
  Real y;
  Real a;
  Real b;
  Real w;
  Real c;
  Real x;
End.

Problem
  w>=0&d>=0&-2<=a&a<=2&b^2>=1/3&w^2*x^2+y^2<=c->[{{x'=y,y'=-w^2*x-2*d*w*y&true}{?x=y*a;w:=2*w;d:=d/2;c:=c*((2*w)^2+1^2)/(w^2+1^2);++?x=y*b;w:=w/2;d:=2*d;c:=c*(w^2+1^2)/(2*w^2+1^2);++?true;}}*]w^2*x^2+y^2<=c
End.


End.

Theorem "Dynamics: Nonlinear 1"

ProgramVariables
  Real x;
  Real a;
End.

Problem
  x^3>=-1->[{x'=(x-3)^4+a&a>=0}]x^3>=-1
End.


End.

Theorem "Dynamics: Nonlinear 2"

ProgramVariables
  Real x1;
  Real a;
  Real x2;
End.

Problem
  x1+x2^2/2=a->[{x1'=x1*x2,x2'=-x1&true}]x1+x2^2/2=a
End.


End.

Theorem "Dynamics: Nonlinear 4"

ProgramVariables
  Real x1;
  Real a;
  Real x2;
End.

Problem
  x1^2/2-x2^2/2>=a->[{x1'=x2+x1*x2^2,x2'=-x1+x1^2*x2&x1>=0&x2>=0}]x1^2/2-x2^2/2>=a
End.


End.

Theorem "Dynamics: Nonlinear 5"

ProgramVariables
  Real x1;
  Real a;
  Real x2;
End.

Problem
  -x1*x2>=a->[{x1'=x1-x2+x1*x2,x2'=-x2-x2^2&true}]-x1*x2>=a
End.


End.

Theorem "Dynamics: Riccati"

ProgramVariables
  Real x;
End.

Problem
  2*x^3>=1/4->[{x'=x^2+x^4&true}]2*x^3>=1/4
End.


End.

Theorem "Dynamics: Nonlinear differential cut"

ProgramVariables
  Real x;
  Real y;
End.

Problem
  x^3>=-1&y^5>=0->[{x'=(x-3)^4+y^5,y'=y^2&true}](x^3>=-1&y^5>=0)
End.


End.

Theorem "STTT Tutorial: Example 1"
Definitions
  Real A();
End.

ProgramVariables
  Real v;
  Real x;
End.

Problem
  v>=0&A()>0->[{x'=v,v'=A()&true}]v>=0
End.


End.

Theorem "STTT Tutorial: Example 2"
Definitions
  Real A();
  Real B();
End.

ProgramVariables
  Real a;
  Real v;
  Real x;
End.

Problem
  v>=0&A()>0&B()>0->[{{a:=A();++a:=0;++a:=-B();}{x'=v,v'=a&v>=0}}*]v>=0
End.


End.

Theorem "STTT Tutorial: Example 3a"
Definitions
  Real S();
  Real A();
  Real B();
End.

ProgramVariables
  Real a;
  Real v;
  Real x;
End.

Problem
  v>=0&A()>0&B()>0&x+v^2/(2*B()) < S()->[{{?x+v^2/(2*B()) < S();a:=A();++?v=0;a:=0;++a:=-B();}{{x'=v,v'=a&v>=0&x+v^2/(2*B())<=S()}++{x'=v,v'=a&v>=0&x+v^2/(2*B())>=S()}}}*]x<=S()
End.


End.

Theorem "STTT Tutorial: Example 4a"
Definitions
  Real V();
  Real A();
End.

ProgramVariables
  Real a;
  Real v;
  Real x;
End.

Problem
  v<=V()&A()>0->[{{?v=V();a:=0;++?v!=V();a:=A();}{x'=v,v'=a&v<=V()}}*]v<=V()
End.


End.

Theorem "STTT Tutorial: Example 4b"
Definitions
  Real V();
  Real A();
End.

ProgramVariables
  Real a;
  Real v;
  Real x;
End.

Problem
  v<=V()&A()>0->[{a:=A();{x'=v,v'=a&v<=V()}}*]v<=V()
End.


End.

Theorem "STTT Tutorial: Example 4c"
Definitions
  Real V();
  Real A();
End.

ProgramVariables
  Real a;
  Real v;
  Real x;
End.

Problem
  v<=V()&A()>0->[{{?v=V();a:=0;++?v!=V();a:=A();}{{x'=v,v'=a&v<=V()}++{x'=v,v'=a&v>=V()}}}*]v<=V()
End.


End.

Theorem "STTT Tutorial: Example 5"
Definitions
  Real S();
  Real A();
  Real ep();
  Real B();
End.

ProgramVariables
  Real a;
  Real v;
  Real c;
  Real x;
End.

Problem
  v>=0&A()>0&B()>0&x+v^2/(2*B())<=S()&ep()>0->[{{?x+v^2/(2*B())+(A()/B()+1)*(A()/2*ep()^2+ep()*v)<=S();a:=A();++?v=0;a:=0;++a:=-B();}c:=0;{x'=v,v'=a,c'=1&v>=0&c<=ep()}}*]x<=S()
End.


End.

Theorem "STTT Tutorial: Example 6"
Definitions
  Real S();
  Real A();
  Real ep();
  Real B();
End.

ProgramVariables
  Real a;
  Real v;
  Real c;
  Real x;
End.

Problem
  v>=0&A()>0&B()>0&x+v^2/(2*B())<=S()&ep()>0->[{{?x+v^2/(2*B())+(A()/B()+1)*(A()/2*ep()^2+ep()*v)<=S();a:=*;?-B()<=a&a<=A();++?v=0;a:=0;++a:=-B();}c:=0;{x'=v,v'=a,c'=1&v>=0&c<=ep()}}*]x<=S()
End.


End.

Theorem "STTT Tutorial: Example 7"
Definitions
  Real S();
  Real A();
  Real ep();
  Real b();
  Real B();
End.

ProgramVariables
  Real a;
  Real v;
  Real c;
  Real x;
End.

Problem
  v>=0&A()>0&B()>=b()&b()>0&x+v^2/(2*b())<=S()&ep()>0->[{{?x+v^2/(2*b())+(A()/b()+1)*(A()/2*ep()^2+ep()*v)<=S();a:=*;?-B()<=a&a<=A();++?v=0;a:=0;++a:=*;?-B()<=a&a<=-b();}c:=0;{x'=v,v'=a,c'=1&v>=0&c<=ep()}}*]x<=S()
End.


End.

Theorem "STTT Tutorial: Example 9a"
Definitions
  Real xr();
  Real Kp();
  Real c();
  Real Kd();
End.

ProgramVariables
  Real v;
  Real x;
End.

Problem
  v>=0&c()>0&Kp()=2&Kd()=3&5/4*(x-xr())^2+(x-xr())*v/2+v^2/4 < c()->[{x'=v,v'=-Kp()*(x-xr())-Kd()*v&true}]5/4*(x-xr())^2+(x-xr())*v/2+v^2/4 < c()
End.


End.

Theorem "STTT Tutorial: Example 9b"
Definitions
  Real S();
  Real Kp();
  Real Kd();
End.

ProgramVariables
  Real xm;
  Real xr;
  Real v;
  Real x;
End.

Problem
  v>=0&xm<=x&x<=S()&xr=(xm+S())/2&Kp()=2&Kd()=3&5/4*(x-xr)^2+(x-xr)*v/2+v^2/4 < ((S()-xm)/2)^2->[{{xm:=x;xr:=(xm+S())/2;?5/4*(x-xr)^2+(x-xr)*v/2+v^2/4 < ((S()-xm)/2)^2;++?true;}{x'=v,v'=-Kp()*(x-xr)-Kd()*v&v>=0}}*]x<=S()
End.


End.

Theorem "STTT Tutorial: Example 10"
Definitions
  Real A();
  Real lw();
  Real ep();
  Real b();
  Real ly();
  Real B();
  Real abs(Real);
End.

ProgramVariables
  Real r;
  Real y;
  Real dx;
  Real a;
  Real v;
  Real w;
  Real c;
  Real dy;
  Real x;
End.

Problem
  v>=0&A()>0&B()>=b()&b()>0&ep()>0&lw()>0&y=ly()&r!=0&dx^2+dy^2=1&abs(y-ly())+v^2/(2*b()) < lw()->[{{?abs(y-ly())+v^2/(2*b())+(A()/b()+1)*(A()/2*ep()^2+ep()*v) < lw();a:=*;?-B()<=a&a<=A();w:=*;r:=*;?r!=0&w*r=v;++?v=0;a:=0;w:=0;++a:=*;?-B()<=a&a<=-b();}c:=0;{x'=v*dx,y'=v*dy,v'=a,dx'=-dy*w,dy'=dx*w,w'=a/r,c'=1&v>=0&c<=ep()}}*]abs(y-ly()) < lw()
End.


End.

Theorem "LICS: Example 1 Continuous car accelerates forward"

ProgramVariables
  Real a;
  Real v;
  Real x;
End.

Problem
  v>=0&a>=0->[{x'=v,v'=a&v>=0}]v>=0
End.


End.

Theorem "LICS: Example 2 Single car drives forward"
Definitions
  Real A();
  Real b();
End.

ProgramVariables
  Real a;
  Real v;
  Real x;
End.

Problem
  v>=0&A()>=0&b()>0->[{{a:=A();++a:=-b();}{x'=v,v'=a&v>=0}}*]v>=0
End.


End.

Theorem "LICS: Example 3a event-triggered car drives forward"

ProgramVariables
  Real a;
  Real m;
  Real b;
  Real v;
  Real A;
  Real x;
End.

Problem
  v>=0&A>=0&b>0->[{{?m-x>=2;a:=A;++a:=-b;}{x'=v,v'=a&v>=0}}*]v>=0
End.


End.

Theorem "LICS: Example 4a safe stopping of time-triggered car"
Definitions
  Real A();
  Real ep();
  Real b();
End.

ProgramVariables
  Real a;
  Real m;
  Real t;
  Real v;
  Real x;
End.

Problem
  v^2<=2*b()*(m-x)&v>=0&A()>=0&b()>0->[{{?2*b()*(m-x)>=v^2+(A()+b())*(A()*ep()^2+2*ep()*v);a:=A();++a:=-b();}t:=0;{x'=v,v'=a,t'=1&v>=0&t<=ep()}}*]x<=m
End.


End.

Theorem "LICS: Example 4b progress of time-triggered car"
Definitions
  Real A();
  Real ep();
  Real b();
End.

ProgramVariables
  Real a;
  Real m;
  Real t;
  Real v;
  Real p;
  Real x;
End.

Problem
  ep()>0&A()>0&b()>0->\forall p \exists m <{{?2*b()*(m-x)>=v^2+(A()+b())*(A()*ep()^2+2*ep()*v);a:=A();++a:=-b();}t:=0;{x'=v,v'=a,t'=1&v>=0&t<=ep()}}*>x>=p
End.


End.

Theorem "LICS: Example 4c relative safety of time-triggered car"
Definitions
  Real A();
  Real ep();
  Real m();
  Real b();
End.

ProgramVariables
  Real a;
  Real t;
  Real v;
  Real x;
End.

Problem
  [{x'=v,v'=-b()&true}]x<=m()&v>=0&A()>=0&b()>0->[{{?2*b()*(m()-x)>=v^2+(A()+b())*(A()*ep()^2+2*ep()*v);a:=A();++a:=-b();}t:=0;{x'=v,v'=a,t'=1&v>=0&t<=ep()}}*]x<=m()
End.


End.

Theorem "LICS: Example 5 Controllability Equivalence"
Definitions
  Real m();
  Real b();
End.

ProgramVariables
  Real v;
  Real x;
End.

Problem
  v>=0&b()>0->(v^2<=2*b()*(m()-x)<->[{x'=v,v'=-b()&true}]x<=m())
End.


End.

Theorem "LICS: Example 6 MPC Acceleration Equivalence"
Definitions
  Real A();
  Real ep();
  Real m();
  Real b();
End.

ProgramVariables
  Real t;
  Real v;
  Real x;
End.

Problem
  v>=0&b()>0&A()>=0&ep()>=0->([t:=0;{x'=v,v'=A(),t'=1&t<=ep()}][{x'=v,v'=-b()&true}]x<=m()<->2*b()*(m()-x)>=v^2+(A()+b())*(A()*ep()^2+2*ep()*v))
End.


End.

Theorem "LICS: Example 7 Model-Predictive Control Design Car"
Definitions
  Real A();
  Real ep();
  Real b();
End.

ProgramVariables
  Real a;
  Real m;
  Real t;
  Real v;
  Real x;
End.

Problem
  [{x'=v,v'=-b()&true}]x<=m&v>=0&A()>=0&b()>0->[{{?[t:=0;{x'=v,v'=A(),t'=1&v>=0&t<=ep()}][{x'=v,v'=-b()&true}]x<=m;a:=A();++a:=-b();}t:=0;{x'=v,v'=a,t'=1&v>=0&t<=ep()}}*]x<=m
End.


End.