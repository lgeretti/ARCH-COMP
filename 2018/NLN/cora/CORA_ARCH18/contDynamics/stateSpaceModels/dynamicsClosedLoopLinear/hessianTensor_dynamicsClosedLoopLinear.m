function Hf=hessianTensor_dynamicsClosedLoopLinear(x,u)



 Hf{1} = sparse(14,14);



 Hf{2} = sparse(14,14);



 Hf{3} = sparse(14,14);

Hf{3}(2,1) = -sin(x(2));
Hf{3}(1,2) = -sin(x(2));
Hf{3}(2,2) = -x(1)*cos(x(2));


 Hf{4} = sparse(14,14);

Hf{4}(2,1) = cos(x(2));
Hf{4}(1,2) = cos(x(2));
Hf{4}(2,2) = -x(1)*sin(x(2));


 Hf{5} = sparse(14,14);



 Hf{6} = sparse(14,14);



 Hf{7} = sparse(14,14);



 Hf{8} = sparse(14,14);



 Hf{9} = sparse(14,14);



 Hf{10} = sparse(14,14);

