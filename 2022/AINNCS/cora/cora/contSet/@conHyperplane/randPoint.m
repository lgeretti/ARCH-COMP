function p = randPoint(hyp,varargin)
% randPoint - generates a random point within an ellipsoid
%
% Syntax:  
%    p = randPoint(obj)
%    p = randPoint(obj,N)
%
% Inputs:
%    obj - ellipsoid object
%    N - number of random points
%
% Outputs:
%    p - random point in R^n
%
% Example: 
%    E = hyp([9.3 -0.6 1.9;-0.6 4.7 2.5; 1.9 2.5 4.2]);
%    p = randPoint(E);
% 
%    figure; hold on;
%    plot(E);
%    scatter(p(1,:),p(2,:),16,'r','filled');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: interval/randPoint

% Author:        Victor Gassmann
% Written:       10-June-2022
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------

% parse input arguments
if ~isa(hyp,'conHyperplane')
    throw(CORAerror('CORA:wrongValue','first',"be a conHyperplane"));
end
if ~(isempty(hyp.C) && all(hyp.d==0))
    throw(CORAerror('CORA:noops','Only implemented for hyperplanes!'));
end

N = 1;
if length(varargin)==1
    N = varargin{1};
end

% get properties (a'*x=b)
a = hyp.h.c;
b = hyp.h.d;
n = length(a);

% normalize a
v = a/norm(a);
c_ = b/norm(a);

% get v'*(x-c) = 0: v'*x = c_ <=> v'(x-v*c_) = 0
c = v*c_;

% compute null space
M = null(v');

% number of vectors in N is equal to n-1
assert(size(M,2)==n-1,'Bug: number of null space vectors wrong!');

Dx = zeros(n,N);
for i=1:N
    % get random linear combination
    facs = randn(1,n-1);
    Dx(:,i) = sum(facs.*M,2);
end

% add back center
p = Dx + c;
%------------- END OF CODE --------------