function res = example_hybrid_reach_ARCH21_rendezvous_SRA05()
% example_hybrid_reach_ARCH21_rendezvous_SRA05 - spacecraft rendezvous
%    benchmark from the ARCH 2021 competition
%
% Syntax:  
%    res = example_hybrid_reach_ARCH21_rendezvous_SRA05()
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% References: 
%   [1] N. Chan et al. "Verifying safety of an autonomous spacecraft 
%       rendezvous mission"  

% Author:       Niklas Kochdumper
% Written:      12-March-2018
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% x_1 = v_x
% x_2 = v_y
% x_3 = s_x 
% x_4 = s_y
% x_5 = t


% Parameter ---------------------------------------------------------------

params.R0 = zonotope([[-900; -400; 0; 0; 0],diag([25; 25; 0; 0; 0])]);
params.startLoc = 1;
params.tFinal = 300;


% Reachability Settings ---------------------------------------------------

% time step
options.timeStep{1} = 2e-1;
options.timeStep{2} = 2e-2;
options.timeStep{3} = 2e-1;

% settings for continuous reachability 
options.zonotopeOrder=10; 
options.taylorTerms=3;

% settings for computing guard intersections
options.guardIntersect = 'nondetGuard';
options.enclose = {'pca'};


% System Dynamics ---------------------------------------------------------

HA = rendezvous_SRA05();


% Specification -----------------------------------------------------------

% randezvous attempt -> check if velocity inside feasible region
C = [0 0 1 0 0;0 0 -1 0 0;0 0 0 1 0;0 0 0 -1 0; ....
     0 0 1 1 0;0 0 1 -1 0;0 0 -1 1 0;0 0 -1 -1 0];
d = [3;3;3;3;4.2;4.2;4.2;4.2];
poly = mptPolytope(C,d);

spec1 = specification(poly,'safeSet');

% randezvous attempt -> check if spacecraft inside line-of-sight
C = [tan(pi/6) -1 0 0 0;tan(pi/6) 1 0 0 0];
d = [0;0];
poly = mptPolytope(C,d);

spec2 = specification(poly,'safeSet');

% passive mode -> check if the space station was hit
spaceStation = interval([-0.1;-0.1],[0.1;0.1]);
func = @(x) checkSpaceStationHit(x,spaceStation);

spec3 = specification(func,'custom');

% specifications + mapping of spec_index to location
spec = add(spec1,spec2);
spec = add(spec,spec3);
spec_location = {[],[1,2],[3]};



% Simulation --------------------------------------------------------------

simRes = simulateRandom(HA,params); 


% Reachability Analysis ---------------------------------------------------

% reachable set computations
timer = tic;
[R,res] = reach(HA,params,options,spec,spec_location);
tComp = toc(timer);

% display results
disp(['specifications verified: ',num2str(res)]);
disp(['computation time: ',num2str(tComp)]);


% Visualiztion ------------------------------------------------------------

figure; hold on; box on

% plot line-of-sight cone
h = fill([-100,0,-100],[-60,0,60],'g','FaceAlpha',0.6,'EdgeColor','none');
set(h,'FaceColor',[0 .6 0]);

% plot reachable set
plot(R,[1,2],'FaceColor',[.6 .6 .6]);

% plot initial set
plot(params.R0,[1,2],'k','FaceColor','w');

% plot simulation
plot(simRes,[1,2]);

% plot space station
plot(spaceStation,[1,2],'FaceColor','r');

xlabel('s_x');
ylabel('s_y');

end


% Auxiliary Functions -----------------------------------------------------

function res = checkSpaceStationHit(S,spaceStation)
% check if the reachable set intersects the space station

   temp = project(S,[1,2]);
   res = 1;
   
   % first check: is interval enclosure intersecting?
   if isIntersecting(spaceStation,interval(temp))
       
       % second check: is reduced zonotope intersecting?
       temp = reduce(temp,'girard',3);
       if isIntersecting(spaceStation,temp)
           res = 0;
       end
   end
end

%------------- END OF CODE --------------