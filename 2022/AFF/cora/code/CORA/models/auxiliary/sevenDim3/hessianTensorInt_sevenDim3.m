function Hf=hessianTensorInt_sevenDim3(x,u)



 out = funOptimize(x,u);



 Hf{1} = interval(sparse(8,8),sparse(8,8));



 Hf{2} = interval(sparse(8,8),sparse(8,8));



 Hf{3} = interval(sparse(8,8),sparse(8,8));

Hf{3}(3,2) = out(1);
Hf{3}(2,3) = out(2);


 Hf{4} = interval(sparse(8,8),sparse(8,8));

Hf{4}(4,3) = out(3);
Hf{4}(3,4) = out(4);


 Hf{5} = interval(sparse(8,8),sparse(8,8));

Hf{5}(5,4) = out(5);
Hf{5}(4,5) = out(6);


 Hf{6} = interval(sparse(8,8),sparse(8,8));



 Hf{7} = interval(sparse(8,8),sparse(8,8));

Hf{7}(7,2) = out(7);
Hf{7}(2,7) = out(8);
function out = funOptimize(in1,uL1R)
%FUNOPTIMIZE
%    OUT = FUNOPTIMIZE(IN1,UL1R)

%    This function was generated by the Symbolic Math Toolbox version 8.7.
%    13-Sep-2021 16:35:35

out = [-4.0./5.0;-4.0./5.0;-1.3e+1./1.0e+1;-1.3e+1./1.0e+1;-1.0;-1.0;-3.0./2.0;-3.0./2.0];
