function Hf=hessianTensor_coupledVanDerPol2(x,u)



 Hf{1} = sparse(5,5);



 Hf{2} = sparse(5,5);

Hf{2}(1,1) = -4*x(2);
Hf{2}(2,1) = -4*x(1);
Hf{2}(1,2) = -4*x(1);


 Hf{3} = sparse(5,5);



 Hf{4} = sparse(5,5);

Hf{4}(3,3) = -4*x(4);
Hf{4}(4,3) = -4*x(3);
Hf{4}(3,4) = -4*x(3);

end