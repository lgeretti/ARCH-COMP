function [A,B]=jacobian_freeParam_vanderPolparamEq(x,u,p)

A=[0,p(1);...
- 2*p(2)*x(1)*x(2) - 1,-p(2)*(x(1)^2 - 1)];

B=[0;...
1];

