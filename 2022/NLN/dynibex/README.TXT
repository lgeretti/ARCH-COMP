## Build the docker
newgrp docker
docker build -t dynibex .

#if not start, dockerd in a terminal

#remark: build compiles all cpp files but does not pre compute anything

## Run the docker

# to obtain results without figures
docker run -v $(pwd)$/output_docker:/benchmarks/results dynibex

# to obtain figures (docker install must be correct)
docker run --net=host --env="DISPLAY" --volume="$HOME/.Xauthority:/root/.Xauthority:rw" -v $(pwd)/output_docker:/benchmarks/results dynibex

# remark: run computes reachable tubes, draws pictures, fills stat files and puts everything in host directory ./output_docker
