function text = example_nonlinear_reach_ARCH22_vanDerPol()
% example_nonlinear_reach_ARCH22_vanDerPol - example of nonlinaer
%    reachability analysis. The guard intersections for the
%    pseudoinvariant are calculated with Girard's method
%
% Syntax:  
%    example_nonlinear_reach_ARCH22_vanDerPol()
%
% Inputs:
%    ---
%
% Outputs:
%    res - true/false
%

% Author:       Mark Wetzlinger
% Written:      06-July-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% choose continuous or hybrid version?
isHybrid = false;

% Parameters --------------------------------------------------------------

params.tFinal = 7; % 7
if isHybrid
    params.startLoc = 1;
end

% original interval for x1,y1,x2,y2: interval([1.25;2.35],[1.55;2.45]);
% original interval for b: 60 to 80

I1 = interval([1.25;2.35],[1.55;2.45]);
B = interval(60,80);
R0 = cartProd(cartProd(I1,I1),B);


% Reachability Settings ---------------------------------------------------

options.timeStep = 0.005;
options.taylorTerms = 3;
options.zonotopeOrder = 100;

options.alg = 'poly';
options.tensorOrder = 3;
options.errorOrder = 10;
options.intermediateOrder = 50;

if strcmp(options.alg,'poly')
    polyZono.maxDepGenOrder = 50;
    polyZono.maxPolyZonoRatio = 0.02;
    polyZono.restructureTechnique = 'reducePca';
%     options.polyZono = polyZono;
    
    params.R0 = polyZonotope(R0);
else
    params.R0 = zonotope(R0);
end

if isHybrid
    options.guardIntersect = 'zonoGirard';
    options.enclose = {'pca'};
end

options.verbose = true;


% System Dynamics ---------------------------------------------------------

sys = nonlinearSys(@coupledVanDerPol_ARCH22,5,1);


% Specifications ----------------------------------------------------------

% original specs: y1 <= 3.7, y2 <= 3.7 should be fulfilled at all times
speclim = 4;

hs1 = halfspace([0 -1 0 0 0],-speclim);
spec = specification(hs1,'unsafeSet');

% hs2 = halfspace([0 0 0 -1 0],-speclim);
% spec = specification({hs1,hs2},'unsafeSet');


% Hybrid Automaton --------------------------------------------------------

if isHybrid
    % artificial guard set
    c = [-0.9808 -0.7286 -0.9808 -0.7286 0]; d = -0.9811;
    guard = conHyperplane(c,d);
    inv1 = mptPolytope(c,d);
    inv2 = mptPolytope([1 0 0 0 0],1000);
    
    % identity reset function
    reset.A = eye(5); reset.c = zeros(5,1);
    
    % instantiate transition
    trans{1} = transition(guard,reset,2);
    
    % instantiate locations
    loc{1} = location('loc1',inv1,trans,sys);
    loc{2} = location('loc2',inv2,[],sys);
    
    % instantiate hybrid automaton for coupled van der Pol system
    HA1 = hybridAutomaton(loc);
end

% Reachability Analysis ---------------------------------------------------

simOptions.points = 20;
simOptions.fracVert = 0.5;
simRes = simulateRandom(sys,params,simOptions);

tic;
if ~isHybrid
    % continuous nonlinear
    [R,res] = reach(sys,params,options,spec);
else
    % hybrid
    [R,res] = reach(HA1,params,options,spec);
end
tComp = toc;
disp(['specifications verified: ', num2str(res)]);
disp(['computation time for van der Pol: ',num2str(tComp)]);


% Visualization -----------------------------------------------------------

% y1 and y2 over time
if ~isHybrid
    figure;
    projDim = [1,3,2,4];
    for i=1:length(projDim)
        if length(projDim) == 2
            subplot(1,2,i); hold on; box on;
        elseif length(projDim) == 4
            subplot(2,2,i); hold on; box on;
        end
    
        % reachable set
        plotOverTime(R,projDim(i));
        plotOverTime(simRes,projDim(i));
    
        % axis limits
        xlim([-0.2,params.tFinal+0.2]);
        ylim([-3,4.05]);
    
        % specs
        plot([0,params.tFinal],[speclim,speclim],'r--');
    
        % formatting
        xlabel('t');
        if projDim(i) == 1
            ylabel("x_1");
        elseif projDim(i) == 2
            ylabel("y_1");
        elseif projDim(i) == 3
            ylabel("x_2");
        elseif projDim(i) == 4
            ylabel("y_2");
        end
    end
end


% x1-y1 and x2-y2
figure;
projDim = {[1,2],[3,4]};

for i=1:length(projDim)
    subplot(1,2,i); hold on; box on;

    % axis limits
    xlim([-2.5,2.5]);
    ylim([-4.05,4.05]);
    
    % reachable set
    plot(R,projDim{i},'FaceColor',colorblind('b'));
    % last set
    plot(R.timePoint.set{end},projDim{i},'EdgeColor','k');
    % simulation
    plot(simRes,projDim{i});
    
    % artificial guard sets
    if isHybrid
        plot(guard,projDim{i},'k');
    end

    % specs
    if i == 1
        plot(conHyperplane(hs1),projDim{i},'r--');
    end
    
    % formatting
    xlabel(['x_' num2str(i)]);
    ylabel(['y_' num2str(i)]);
end

text = ['CORA,CVDP22, ,',num2str(res),',',num2str(tComp)];

%------------- END OF CODE --------------